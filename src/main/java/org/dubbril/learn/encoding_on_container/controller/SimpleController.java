package org.dubbril.learn.encoding_on_container.controller;

import lombok.extern.slf4j.Slf4j;
import org.dubbril.learn.encoding_on_container.domain.Person;
import org.dubbril.learn.encoding_on_container.service.SimpleService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/person")
public class SimpleController {

    private final SimpleService simpleService;

    public SimpleController(SimpleService simpleService) {
        this.simpleService = simpleService;
    }

    @PostMapping(value = "new", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> process(@RequestBody Person person) {
        log.info("Request Body ==> {}", person);
        Person process = simpleService.process(person);
        return ResponseEntity.ok().body(process);
    }
}
