# 1. Build & Run Docker
    mvn clean install
    docker build -t poc-encoding:latest .
    docker run -d --name test-encoding -p 8080:8080 poc-encoding:latest
    docker exec -it test-encoding bash

- 1.1 on terminal cd to directory of project for run
```
mvn clean install ; docker build -t poc-encoding:latest . ; docker run -d --name test-encoding -p 8080:8080 poc-encoding:latest
```
# 2. Clean Docker image
```
docker stop test-encoding ; docker rm test-encoding ; docker rmi poc-encoding
```
# 3. Curl Endpoint
```
curl --location --request POST 'http://localhost:8080/api/v1/person/new' \
--header 'Content-Type: application/json' \
--data '{
    "age": 40,
    "name": "แพรี่",
    "surname": "เจ้าตุ่นปากเป็ด",
    "address": {
        "addressLocation": "999/999",
        "moo": "9",
        "tambon": "ปากเกร็ด",
        "amphur": "ปากเกร็ด",
        "province": "นนทบุรี"
    }
}'
```

<div style="text-align:center">
    <img src="./pngegg2.png" alt="Local Image" />
</div>




