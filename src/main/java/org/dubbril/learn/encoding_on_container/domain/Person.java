package org.dubbril.learn.encoding_on_container.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Person {
    private int age;
    private String name;
    private String surname;
    private Address address;

    @Override
    public String toString() {
        return "Person{" +
                "age='" + age + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", address=" + address +
                '}';
    }
}
