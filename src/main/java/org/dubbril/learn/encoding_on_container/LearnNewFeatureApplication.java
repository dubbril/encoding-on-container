package org.dubbril.learn.encoding_on_container;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.charset.Charset;

@Slf4j
@SpringBootApplication
public class LearnNewFeatureApplication {
    public static void main(String[] args) {
        Charset defaultCharset = Charset.defaultCharset();
        log.info("Default Charset : {}", defaultCharset);
        SpringApplication.run(LearnNewFeatureApplication.class, args);
    }
}
