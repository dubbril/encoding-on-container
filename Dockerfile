FROM registry.access.redhat.com/ubi8/openjdk-11-runtime:1.16

ENV LANGUAGE='en_US:en'

COPY target/*.jar /deployments/app.jar

EXPOSE 8080
USER 185

ENV AB_JOLOKIA_OFF=""
ENV JAVA_APP_JAR="/deployments/app.jar"