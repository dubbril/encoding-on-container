package org.dubbril.learn.encoding_on_container.service;

import org.dubbril.learn.encoding_on_container.domain.Person;
import org.springframework.stereotype.Service;

@Service
public class SimpleService {

    public Person process(Person person) {
        byte[] nameBytes = person.getName().getBytes();
        byte[] surNameBytes = person.getSurname().getBytes();
        return Person.builder()
                .age(person.getAge())
                .name(new String(nameBytes))
                .surname(new String(surNameBytes))
                .address(person.getAddress())
                .build();
    }
}
