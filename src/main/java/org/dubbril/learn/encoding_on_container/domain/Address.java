package org.dubbril.learn.encoding_on_container.domain;

import lombok.Data;

@Data
public class Address {
    private String addressLocation;
    private String moo;
    private String tambon;
    private String amphur;
    private String province;

    @Override
    public String toString() {
        return addressLocation + ", moo " + moo + ", tambon " + tambon + ", amphur " + amphur + ", province " + province;
    }
}
